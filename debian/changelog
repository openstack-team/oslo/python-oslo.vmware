python-oslo.vmware (4.6.0-2) experimental; urgency=medium

  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 16:49:31 +0100

python-oslo.vmware (4.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Feb 2025 17:55:28 +0100

python-oslo.vmware (4.5.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090583).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 08:25:26 +0100

python-oslo.vmware (4.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 14:30:54 +0200

python-oslo.vmware (4.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 17:38:49 +0200

python-oslo.vmware (4.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:50:58 +0200

python-oslo.vmware (4.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 23:07:46 +0100

python-oslo.vmware (4.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 14:19:35 +0200

python-oslo.vmware (4.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Added python3-defusedxml as (b-)depends.
  * Cleans even better.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 15:30:45 +0200

python-oslo.vmware (4.1.1-3) unstable; urgency=medium

  * Cleans better (Closes: #1045357).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Aug 2023 14:23:20 +0200

python-oslo.vmware (4.1.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 11:50:07 +0200

python-oslo.vmware (4.1.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 11:50:00 +0100

python-oslo.vmware (4.0.1-3) unstable; urgency=medium

  * Removed python3-mox3 from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Dec 2022 18:30:49 +0100

python-oslo.vmware (4.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:01:27 +0200

python-oslo.vmware (4.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 17:09:03 +0200

python-oslo.vmware (3.10.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 09:40:17 +0100

python-oslo.vmware (3.10.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 12:35:50 +0100

python-oslo.vmware (3.9.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:07:48 +0200

python-oslo.vmware (3.9.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 15:10:39 +0200

python-oslo.vmware (3.8.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 09:28:24 +0200

python-oslo.vmware (3.8.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed version when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 09:42:31 +0100

python-oslo.vmware (3.7.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 10:29:39 +0200

python-oslo.vmware (3.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed min version of python3-lxml (>= 4.5.0).

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 15:55:10 +0200

python-oslo.vmware (3.6.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 21:52:35 +0200

python-oslo.vmware (3.3.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 22:29:39 +0200

python-oslo.vmware (3.3.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 16:06:43 +0200

python-oslo.vmware (2.34.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:00:22 +0200

python-oslo.vmware (2.34.1-1) experimental; urgency=medium

  * New upstream release.
  * Added python3-sphinxcontrib.apidoc as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Sep 2019 10:13:21 +0200

python-oslo.vmware (2.34.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Sep 2019 14:43:17 +0200

python-oslo.vmware (2.32.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 00:53:35 +0200

python-oslo.vmware (2.32.2-1) experimental; urgency=medium

  * New upstream release.
  * Removed inactive uploaders.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Fri, 22 Mar 2019 10:51:59 +0100

python-oslo.vmware (2.31.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 23:22:21 +0200

python-oslo.vmware (2.31.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using Python 3 for building sphinx doc.
  * Now using pkgos-dh_auto_test.
  * Blacklist failing tests in Python 3.7:
    - ExceptionsTest.test_vim_fault_exception_with_cause_and_details()
    - ExceptionsTest.test_vim_fault_exception()

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Aug 2018 18:11:55 +0200

python-oslo.vmware (2.26.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:29:39 +0000

python-oslo.vmware (2.26.0-1) experimental; urgency=medium

  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Feb 2018 13:25:50 +0000

python-oslo.vmware (2.23.0-4) unstable; urgency=medium

  * Uploading to unstable.
  * Add missing python-openstackdocstheme b-d (Closes: #880399).

 -- Thomas Goirand <zigo@debian.org>  Tue, 31 Oct 2017 09:49:15 +0000

python-oslo.vmware (2.23.0-3) experimental; urgency=medium

  * Add missing b-d: python3-ddt (Closes: #877614).

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Oct 2017 03:59:29 +0000

python-oslo.vmware (2.23.0-2) experimental; urgency=medium

  * Add missing (build-)depends python{3,}-lxml (Closes: #877254).

 -- Thomas Goirand <zigo@debian.org>  Sun, 01 Oct 2017 19:01:25 +0000

python-oslo.vmware (2.23.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed now useless transition packages.
  * Now using pkgos-dh_auto_install.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Sep 2017 22:18:03 +0200

python-oslo.vmware (2.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 10:17:36 +0000

python-oslo.vmware (2.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 03:10:30 +0000

python-oslo.vmware (2.2.1-2) experimental; urgency=medium

  * Added Python 3 support.
  * Using https VCS urls.
  * Fixed homepage field.
  * Fixed debian/copyright ordering and years and holders.

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Feb 2016 03:55:49 +0000

python-oslo.vmware (2.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Jan 2016 09:20:02 +0000

python-oslo.vmware (2.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 23:05:35 +0000

python-oslo.vmware (2.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Dec 2015 11:12:19 +0100

python-oslo.vmware (1.21.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Also packaging the oslo_vmware/wsdl directory.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 20:54:08 +0000

python-oslo.vmware (1.21.0-1) experimental; urgency=medium

  [ James Page ]
  * d/pydist-overrides: Map suds-jurko -> suds to ease backporting.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 21:59:45 +0200

python-oslo.vmware (1.18.0-2) experimental; urgency=medium

  * Removed python-bandit build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 20:55:09 +0000

python-oslo.vmware (1.18.0-1) experimental; urgency=medium

  [ James Page ]
  * Fixup typo in transitional package description (LP: #1471561).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Transitional packages are of priority: extra.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 20:33:39 +0000

python-oslo.vmware (0.13.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream release:
    - d/control: Align version requirements and dependencies with upstream.
  * Re-align with Ubuntu:
    - d/control: Add Breaks/Replaces and transitional packages for
      *oslo-vmware packages in Ubuntu.
  * d/*: wrap-and-sort.

 -- James Page <james.page@ubuntu.com>  Tue, 09 Jun 2015 08:52:14 +0100

python-oslo.vmware (0.11.1-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Reviewed (build-)depends for this release.
  * Removed sphinx conf.py patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Dec 2014 22:16:09 +0800

python-oslo.vmware (0.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Uploading to Experimental right before the freeze.
  * Patches doc/source/conf.py which is failing the sphinx doc build.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Sep 2014 13:43:21 +0800

python-oslo.vmware (0.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Updated (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Jul 2014 21:26:37 +0800

python-oslo.vmware (0.3-2) unstable; urgency=medium

  * Dropped python-hacking from b-d (just to make James Page happy... :).

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Jun 2014 01:06:22 +0800

python-oslo.vmware (0.3-1) unstable; urgency=medium

  [ Thomas Bechtold ]
  * debian/watch: Use github tags.

  [ Thomas Goirand ]
  * New upstream release.
  * Uploading to unstable.
  * Fixed UPSTREAM_GIT url in debian/rules.
  * Fixed new (build-)requirements for this release.
  * Using testr and subunit properly.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Apr 2014 05:45:49 +0000

python-oslo.vmware (0.2-1) experimental; urgency=low

  * Initial release. (Closes: #740806)

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Mar 2014 15:29:17 +0800
